# Daily emotional dynamics a comparison of networks

L'objectif de ce projet est de comparer la prise en compte différenciée de l'hétérogénéité individuelle au sein de deux modèles en réseaux de groupe sur données à mesures intensives de la dynamique affective de sujets tout venant .